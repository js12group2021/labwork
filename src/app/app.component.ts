import { Component } from '@angular/core';
import {CardDeck} from "../lib/CardDesk";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  Rank = "K";
  Suit = "clubs";

  carddeck = new CardDeck();
  currentDeck = this.carddeck.getCards(2);
}
