import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {

@Input() rank: string = '';
@Input() suit: string = '';

  getSymbol(){
    if(this.suit === 'diams') return'♦';
    if(this.suit === 'hearts') return '♥';
    if(this.suit === 'clubs') return '♣';
    if(this.suit === 'spades') return '♠';
    else return -1;
  }
}
