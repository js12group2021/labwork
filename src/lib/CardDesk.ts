

export class Card {
  public rank:string[] = [];
  public suit:string[] = [];
  constructor(suit:string[], rank:string[]) {
   this.rank = rank;
   this.suit = suit;
  }

  getScore(rank:string) {
    let value = 0;
    if (rank === 'J' || 'Q' || 'K') {
      value = 10;
    } else if (rank === 'A') {
      value = 11;
    } else {
      value = parseInt(rank);
    }
    return value;
  }
}





export class CardDeck{
  public  Cards:Card[] = [];
  constructor() {
    const RANKS = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
    const SUITS = ["diams", "hearts", "clubs", "spades"];


    for (let suit = 0; suit < SUITS.length; suit++) {
      for (let number = 0; number < RANKS.length; number++) {
        let t = new Card(SUITS, RANKS);
        this.Cards.push(t);
      }
    }


  }


  getCard(){

      let randomNum = Math.floor(Math.random() * this.Cards.length);

      const deletedCard = this.Cards.splice(randomNum, 1);

      const card = deletedCard[0];

      return card;
  }


  getCards(howMany: number){
    const temp = [];

    for (let i = 0; i < howMany; i++){
      temp.push(this.getCard());
    }
    return temp;
  }


}
